resource "azurerm_linux_virtual_machine" "vm" {
  name                  = var.vm_name
  location              = var.location
  resource_group_name   = var.resource_group_name
  network_interface_ids = [var.nic_id]
  size                  = "Standard_B2s"

  admin_username = "my_username_ssh"
  admin_ssh_key {
    username   = "my_username_ssh"
    public_key = file("../../modules/VirtualMachine/dat-sydney.pub")
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  os_disk {
    caching           = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  custom_data = base64encode(data.template_file.user_data.rendered)
}

data "template_file" "user_data" {
  template = file("../../modules/VirtualMachine/user_data.sh")
}