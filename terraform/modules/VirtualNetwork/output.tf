output "vnet-name" {
  value = azurerm_virtual_network.myvnet.name
}

output "vnet_id" {
  value = azurerm_virtual_network.myvnet.id
}