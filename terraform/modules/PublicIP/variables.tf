variable "location" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "ip_name" {
  type = string
}