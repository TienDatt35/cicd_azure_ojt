variable "location" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "publicIP" {
  type = string
}

variable "nic_name" {
  type = string
}
