variable "resource_group_name" {
  type = string
}

variable "resource_group_name_vm" {
  type = string
}

variable "location" {
  type = string
}

variable "storage_account_name" {
  type = string
}

variable "container_name" {
  type = string
}

variable "key" {
  type = string
}

variable "vm_name" {
  type = list(string)
}