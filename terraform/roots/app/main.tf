terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.49.0"
    }
  }
  backend "azurerm" {
  }
}

provider "azurerm" {
  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

data "terraform_remote_state" "infa" {
  backend = "azurerm"

  config = {
    resource_group_name  = var.resource_group_name
    storage_account_name = var.storage_account_name
    container_name       = var.container_name
    key                  = var.key
  }
}

locals {
  nic = data.terraform_remote_state.infa.outputs.nic
}

# output "test_nic" {
#   value = local.nic
# }

module "vm" {
  count = length(local.nic)
  source = "../../modules/VirtualMachine"
  vm_name = var.vm_name[count.index]
  location = var.location
  resource_group_name = var.resource_group_name_vm
  nic_id = local.nic[count.index].nic_id
}

# local.nic[0].nic_id
# module "publicIP" {
#   source = "../../modules/PublicIP"
#   location = var.location
#   resource_group_name = var.resource_group_name
# }

# module "NIC" {
#   source = "../../modules/NetworkInterface"
#   location = var.location
#   resource_group_name = var.resource_group_name
#   subnet_id = local.public_subnets_ids
#   publicIP = module.publicIP.publicIP_id
# }