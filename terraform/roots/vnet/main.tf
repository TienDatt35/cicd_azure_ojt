terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.49.0"
    }
  }
  backend "azurerm" {
  }
}

provider "azurerm" {
  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

module "RG" {
  source = "../../modules/ResourceGroup"
  resource_group_name = var.resource_group_name
  location = var.location
}

module "Vnet" {
  source = "../../modules/VirtualNetwork"
  virtual_network_name = var.virtual_network_name
  address_space = var.address_space
  vnet_location = var.location
  resource_group_name = var.resource_group_name
}

module "subnet" {
  source = "../../modules/Subnets"
  for_each = var.subnets
  subnet_name = each.value.subnet_name
  resource_group_name = var.resource_group_name
  virtual_network_name = module.Vnet.vnet-name
  subnet_prefix = each.value.subnet_prefix
  nsg_assocation = module.nsg.nsg_id
}

module "nsg" {
  source = "../../modules/NetworkSG"
  location = var.location
  resource_group_name = var.resource_group_name
}

module "publicIP" {
  source = "../../modules/PublicIP"
  count = 2
  ip_name = var.ip_name[count.index]
  location = var.location
  resource_group_name = var.resource_group_name
}
output "publicIP" {
  value = module.publicIP
}

output "subnet" {
  value = module.subnet
}
variable "nic_subnet" {
  
}
module "NIC" {
  source = "../../modules/NetworkInterface"
  count = length(var.nic_subnet)
  location = var.location
  nic_name = var.nic_name[count.index]
  resource_group_name = var.resource_group_name
  subnet_id = module.subnet[var.nic_subnet[count.index]].subnet_id
  publicIP = module.publicIP[count.index].publicIP_id
}



