# resource gr
variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}

# vnet
variable "virtual_network_name" {
  type = string
}

variable "address_space" {
  type = list(string)
}


variable "ip_name" {
  type = list(string)
}


#subnets
variable "subnets" {
  type = map(object({
    subnet_name    = string
    subnet_prefix   = string
  }))
}

variable "nic_name" {
  type = list(string)
}

