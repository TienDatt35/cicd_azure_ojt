resource_group_name = "my-rg"
location = "East Asia"
virtual_network_name = "my-vnet"
address_space = ["10.0.0.0/16"]

# subnet_name = ["pri-subnet", "pub-subnet"]
# subnet_prefix = ["10.0.1.0/24", "10.0.2.0/24"]

ip_name = ["pub_ip1", "pub_ip2"]

nic_subnet = ["subnet_1", "subnet_2"]

nic_name = ["nic1", "nic2"]

subnets = {
    "subnet_1" = {
        subnet_name = "pri-subnet"
        subnet_prefix = "10.0.1.0/24"
    }
    "subnet_2" = {
        subnet_name = "pub-subnet"
        subnet_prefix = "10.0.2.0/24"
    }
}